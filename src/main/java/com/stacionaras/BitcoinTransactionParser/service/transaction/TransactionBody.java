package com.stacionaras.BitcoinTransactionParser.service.transaction;

import org.bitcoinj.core.Sha256Hash;

import java.math.BigInteger;

public class TransactionBody {
    private BigInteger transactionId;
    private Sha256Hash transactionHash;
    private int transactionVersion;
    private BigInteger transactionLockTime;
    private BigInteger transactionSize;

    public TransactionBody() {
    }

    public TransactionBody(BigInteger transactionId, Sha256Hash transactionHash,
                           int transactionVersion, BigInteger transactionLockTime, BigInteger transactionSize) {
        this.transactionId = transactionId;
        this.transactionHash = transactionHash;
        this.transactionVersion = transactionVersion;
        this.transactionLockTime = transactionLockTime;
        this.transactionSize = transactionSize;
    }

    public BigInteger getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(BigInteger transactionId) {
        this.transactionId = transactionId;
    }

    public Sha256Hash getTransactionHash() {
        return transactionHash;
    }

    public void setTransactionHash(Sha256Hash transactionHash) {
        this.transactionHash = transactionHash;
    }

    public int getTransactionVersion() {
        return transactionVersion;
    }

    public void setTransactionVersion(int transactionVersion) {
        this.transactionVersion = transactionVersion;
    }

    public BigInteger getTransactionLockTime() {
        return transactionLockTime;
    }

    public void setTransactionLockTime(BigInteger transactionLockTime) {
        this.transactionLockTime = transactionLockTime;
    }

    public BigInteger getTransactionSize() {
        return transactionSize;
    }

    public void setTransactionSize(BigInteger transactionSize) {
        this.transactionSize = transactionSize;
    }
}

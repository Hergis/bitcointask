package com.stacionaras.BitcoinTransactionParser;

import com.stacionaras.BitcoinTransactionParser.server.data.access.repository.datFile.ParseDatFile;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class BTPapp implements CommandLineRunner {


    private ParseDatFile parseDatFile;
    private String userInput;

    public void run(String... args) {
        Scanner user_input = new Scanner(System.in);
        System.out.println("Enter blocks hash of which transactions you want to find");
        userInput = user_input.next();

        List<String> userInputArray = Arrays.asList(userInput.split(","));

        parseDatFile.findTransactionsInDatFile(userInputArray);


    }

    public static void main(String[] args) {
        SpringApplication.run(BTPapp.class, args);
    }
}

package com.stacionaras.BitcoinTransactionParser.server.data.access.repository.datFile;

import com.stacionaras.BitcoinTransactionParser.server.data.access.repository.database.transaction.TransactionRepository;
import org.bitcoinj.core.Block;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.utils.BlockFileLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ParseDatFile {

    private TransactionRepository transactionRepository;

    public void findTransactionsInDatFile(List<String> blockNumbers) {

        NetworkParameters np = new MainNetParams();
        List<File> blockChainFiles = new ArrayList<>();
        blockChainFiles.add(new File("/tmp/bootstrap.dat"));
        BlockFileLoader blockFileLoader = new BlockFileLoader(np, blockChainFiles);

// Iterate over the blocks in the dataset.
        for (String blockNumber : blockNumbers) {
            for (Block block : blockFileLoader) {
                if (blockNumber.equals(block.getHashAsString())) {
                    transactionRepository.insertTransaction(block.getTransactions());
                }
            }
        }
    }
}
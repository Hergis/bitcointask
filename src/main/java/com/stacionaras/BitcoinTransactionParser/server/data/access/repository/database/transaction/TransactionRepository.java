package com.stacionaras.BitcoinTransactionParser.server.data.access.repository.database.transaction;

import org.bitcoinj.core.Transaction;

import java.sql.*;
import java.util.List;

public class TransactionRepository {

    private static final String DB_DRIVER = "org.postgresql.Driver";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/BitcoinDB";
    private static final String DB_USERNAME = "postgres";
    private static final String DB_PASSWORD = "postgres";

    private static final String INSERT_TRANSACTION = "INSERT INTO transactions(transaction_hash, transaction_version, transaction_lockTime) VALUES (?,?,?)";


    public void insertTransaction(List<Transaction> transactions) {

        if (!transactions.isEmpty()) {
            for (Transaction transaction : transactions) {
                System.out.println("INSERT INTO transaction(transaction_hash, transaction_version, transaction_lockTime)" +
                        " VALUES(" + transaction.getHashAsString() + "," + transaction.getVersion() + "," + transaction.getLockTime() + ")");
                PreparedStatement statement = null;
                try (Connection connection = getConnection()) {
                    statement = connection.prepareStatement(INSERT_TRANSACTION);
                    statement.setString(1, transaction.getHashAsString());
                    statement.setLong(2, transaction.getVersion());
                    statement.setLong(3, transaction.getLockTime());
                    statement.executeUpdate();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } finally {
                    closeStatement(statement);
                }
            }
        }
    }

    public TransactionRepository() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    private static Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
